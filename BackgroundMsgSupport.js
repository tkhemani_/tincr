/*
* Copyright 2012 Ryan Ackley (ryanackley@gmail.com)
*
* This file is part of Tincr.
*
* Tincr is free software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

var backgroundMsgSupport = {
	launchFolderSelect : function(index, url,  callback){
		chrome.extension.sendRequest({key: 'launchFolderSelect', index: index, url: url}, callback);
	},
	launchFileSelect : function(url, op, callback){
		chrome.extension.sendRequest({key: 'launchFileSelect', url: url, op: op}, callback);
	},
	getProjectTypes : function(callback){
		chrome.extension.sendRequest({key : 'ProjectTypes'}, callback);
	},
	checkResources : function(resources, callback){
		chrome.extension.sendRequest({key : 'checkResources', resources: resources}, callback);
	},
	checkResourceContent : function(url, content, callback){
		chrome.extension.sendRequest({key : 'checkResourceContent', url: url, content:content}, callback);
	},
	updateResource : function(url, content, callback){
		chrome.extension.sendRequest({key : 'updateResource', url: url, content:content}, callback);
	},
	pageChanged : function(callback){
		chrome.extension.sendRequest({key: 'pageChanged'}, callback);
	},
	unwatchDirectory : function(callback){
		chrome.extension.sendRequest({key: 'unwatchDirectory'}, callback); 
	},
	loadProject : function(type, path, url, urlPathMap, callback){
		chrome.extension.sendRequest({key: 'loadProject', type: type, path: path, url: url, urlPathMap: urlPathMap}, callback);
	},
	setResourceOptions : function(url, exactMatch, callback){
		chrome.extension.sendRequest({key: 'setResourceOptions', url: url, exactMatch: exactMatch}, callback);
	},
	removeDependency : function(url, index, callback){
		chrome.extension.sendRequest({key: 'removeDependency', url: url, index: index}, callback);
	},
	setCustomPathData : function(url, data, callback){
		chrome.extension.sendRequest({key: 'setCustomPathData', url:url, data: data}, callback);
	},
	clearResource : function(url, callback){
		chrome.extension.sendRequest({key: 'clearResource', url:url}, callback);
	},
	getPreference : function(prefKey, callback){
		chrome.extension.sendRequest({key:'getPreference', prefKey: prefKey}, callback);
	},
	setPreference : function(prefObj, callback){
		chrome.extension.sendRequest({key: 'setPreference', prefObj: prefObj}, callback);
	}
}
