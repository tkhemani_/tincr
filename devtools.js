/*
* Copyright 2012 Ryan Ackley (ryanackley@gmail.com)
*
* This file is part of Tincr.
*
* Tincr is free software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
// Most of the logic happens in devtools.html since it loads right away
var projectState = {};
var inspectedLocation;
var watchPort;

var saveProjectState = function(){
	var storeData = {};
	if (projectState.type != 'fileUrl'){
		storeData[inspectedLocation.origin] = projectState;
		backgroundMsgSupport.setPreference(storeData, function(){});
	}
	else{
		var temp = {autosave: projectState.autosave, watchFiles: projectState.watchFiles};
		storeData[inspectedLocation.origin] = temp;
		backgroundMsgSupport.setPreference(storeData, function(){});
	}
}

var saveMapping = function(url, path){
	projectState.urlDataMap = projectState.urlDataMap || {};
	projectState.urlDataMap[url] = path;
	saveProjectState();
};

var saveFullReloadMapping = function(url, isFullReload){
	projectState.fullReloadMap = projectState.fullReloadMap || {};
	projectState.fullReloadMap[url] = isFullReload;
	saveProjectState();
};

var isFullReload = function(url, type){
	projectState.fullReloadMap = projectState.fullReloadMap || {};
	var isSet = projectState.fullReloadMap[url];
	if (type == 'script' && undefined == isSet){
		return true;
	}
	else{
		return isSet;
	}
}

var getMapping = function(url){
	projectState.urlDataMap = projectState.urlDataMap || {};
	return projectState.urlDataMap[url] || {};
}
var loadProject = function(type, path, autosave, watchFiles, urlDataMap, fullReloadMap){
	checkResources(urlDataMap);
	registerNavListener();
	
	toggleWatchingFiles(watchFiles, path);
	projectState = {type: type, path: path, autosave:autosave, watchFiles:watchFiles, urlDataMap: urlDataMap, fullReloadMap:fullReloadMap};
	saveProjectState();
};

var toggleWatchingFiles = function(enable, path, callback){
	projectState.watchFiles = enable;
	var registerWatch = function(){
		if (enable){
			watchPort = chrome.extension.connect({name:path});
			watchPort.onMessage.addListener(fileChangeListener);
		}
		if (callback){
			callback();
		}	
	}
	
	if (watchPort){
		watchPort.disconnect();
		backgroundMsgSupport.unwatchDirectory(function(){
			registerWatch();
		});
	}
	else{
		registerWatch();
	}
	
}

var checkLocation = function(){
	chrome.devtools.inspectedWindow.eval('this.location', function(location, isException){
		if (inspectedLocation && location.origin === inspectedLocation.origin){
			return;
		}
		inspectedLocation = location;
		// file protocol is a special case
		var type,path,temp;
		var maybeLoadProject = function(){
			var urlDataMap = temp.urlDataMap || {};
			var fullReloadMap = temp.fullReloadMap || {}
			if (type && path && projectState.path != path){
				backgroundMsgSupport.loadProject(type, path, inspectedLocation.origin, urlDataMap, function(data){
					if (data.error){
						logError(data.error);
					}
					else{
						loadProject(type, path, temp.autosave, temp.watchFiles, urlDataMap, fullReloadMap);
					}
				});
			}
		}
		if (location.protocol == 'file:'){
			var pathElements = location.pathname.split("/");
			type = 'fileUrl';
			path = '';
			// get the root directory of the currently viewed html page.
			for (var i = 1; i < pathElements.length - 1; i++){
				path = path + '/' + pathElements[i];
			}
			if (path.charAt(0) == '/' && navigator.platform.indexOf('Win') == 0){
				path = path.substring(1);
			}
			backgroundMsgSupport.getPreference(String(location.origin), function(data){
				temp = data[location.origin] || {autosave: true, watchFiles: true};
				maybeLoadProject();
			});
		}
		else{
			backgroundMsgSupport.getPreference(String(location.origin), function(data){
				temp = data[location.origin];
				if (temp){
					if (temp.type != 'fileUrl'){
						type = temp.type;
						path = temp.path;
					}
					maybeLoadProject();
				}
			});
		}
		 
	});
}
//window.setTimeout(function(){
checkLocation();
//}, 10000);

backgroundMsgSupport.getPreference('logging', function(data){
	toggleLogging(data['logging'] === 'true');
});

$(window).bind('resourceRefresh', function(){
	if (!window.editorPanelWindow){
		return;
	}
	editorPanelWindow.clearResources();
});

$(window).bind('resourceItem', function(e, resources){
	if (!window.editorPanelWindow){
		return;
	}
	editorPanelWindow.refreshResources(resources);
});
$(window).bind('resourceConfirm', function(e, resource, isUserMapped){
	if (!window.editorPanelWindow){
		return;
	}
	editorPanelWindow.updateResource(resource, isUserMapped);
});

chrome.devtools.panels.create(' Tincr ', 'icon.png', 'editorpanel.html', function(panel){
	var windowInjector = function(panelWindow){
		window.editorPanelWindow = panelWindow;
		panelWindow.devtoolsWindow = window;
		panelWindow.initUI();
		panel.onShown.removeListener(windowInjector);
	};
	panel.onShown.addListener(windowInjector);
});
