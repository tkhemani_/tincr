var projectTypes;
var projectState; 

var checkProjectState = function(){
    projectState = window.devtoolsWindow.projectState;
        
    if (projectState.type == 'fileUrl'){
        $('#project-type').attr('disabled', 'true');
        $('#native-browse').attr('disabled', 'true');
        
        $('#file-select').hide();
        $('#auto-refresh').hide();
        $('#auto-save').hide();
        $('#resource-section').show();
    }
    else if (projectState.type){
        var typeSelect = document.getElementById('project-type');
        $('#project-type').removeAttr('disabled');
        $('#native-browse').removeAttr('disabled');
        
        for (var i = 0; i < projectTypes.length; i++){
            if (projectTypes[i].key === projectState.type){
                typeSelect.value = String(i);
                break;
            }
        }
        $('#local-file-path').text(projectState.path).show();
        $('#toggle-watch')[0].checked = projectState.watchFiles;
        $('#toggle-autosave')[0].checked = projectState.autosave;
        
        $('#file-select').show();
        $('#auto-refresh').show();
        $('#auto-save').show();
        
        $('#load-error').hide();
        $('#error-img').hide();
        $('#success-img').show();
        $('#load-success').show();
        $('#resource-section').show();
    }
    
    backgroundMsgSupport.getPreference('logging', function(data){
        var loggingEnabled = data['logging'];
        toggleLogging(loggingEnabled);
        $('#toggle-logging')[0].checked = loggingEnabled;
    })
    
    
}
var clearResources = function(){
    $('#resource-list-container').empty();
};
var updateResource = function(resource, isUserMapped){
    var paths = resource.paths || {};
    var $item = $('.resource[data-url="' + resource.url +'"]');
    updateResourceData(resource.url, $item, paths, resource.type, isUserMapped);
};

var updateResourceData = function(url, $item, paths, docType, isUserMapped){
    if (isUserMapped){
        $item.addClass('user-mapped');
    }
    else{
        $item.removeClass('user-mapped');
    }

    if (paths.autoSave){
        $('.resource-localpath span', $item).text(paths.autoSave);
    }
    else if (paths.autoReload && paths.autoReload.length){
        $('.resource-localpath span', $item).text(paths.autoReload[0]);   
    }
    else{
        $('.resource-localpath span', $item).text('None');
    }
    $('.auto-refresh-item', $item).remove();
    $('.resource-reloadfrom', $item).hide();
    if (paths.autoReload && paths.autoReload.length > 1){ 
        $('.resource-reloadfrom', $item).show();
        for (var j = 1; j < paths.autoReload.length; j++){
            $('.resource-reloadfrom ul', $item).append('<li class="auto-refresh-item">' + paths.autoReload[j] + '&nbsp;<a data-index="' + j + '" href="#" class="remove-file">Remove</a></li>');
        }
    }
    projectState = devtoolsWindow.projectState;
    if (projectState && (projectState.type == 'config.file' || projectState.type == 'fileUrl')){
        $('.resource-actions', $item).hide();
        $('.resource-options', $item).hide();
    }
    else{
        updateResourceActions(url, $item, paths, docType);
        updateResourceOptions(url, $item, paths, docType);
    }
    

};

var updateResourceActions = function(url, $item, paths, docType){
    var $actions = $('.resource-actions', $item);
    $actions.empty();

    if (paths.autoReload || paths.autoSave){
        $actions.append('<a href="#" class="browse-autosave-file">Change File Path</a><span> | </span><a href="#" class="clear-autosave-file">Clear File Path</a>');
        if (paths.autoReload && paths.autoReload.length){
           $actions.append('<span> | </span><a href="#" class="browse-add-autorefresh-file">Add Dependency</a>'); 
        } 
    }
    else{
        $actions.append('<a href="#" class="browse-autosave-file">Set File Path</a>');
    }

    var data = paths;
    $('.clear-autosave-file', $item).click(function(){
        backgroundMsgSupport.clearResource(url, function(data){
            updateResourceData(url, $item, data, docType, true);
            window.devtoolsWindow.saveMapping(url, data);
        });
        return false;
    });
    $('.browse-autosave-file', $item).click(function(){
        backgroundMsgSupport.launchFileSelect(url, 'autosave', function(data){
            //$('.resource-localpath span', $item).text(data.localPath);
            updateResourceData(url, $item, data, docType, true);
            window.devtoolsWindow.saveMapping(url, data);
        });
        return false;
    });

    // deal with adding a file for auto refresh
    $('.browse-add-autorefresh-file', $item).click(function(){
        backgroundMsgSupport.launchFileSelect(url, 'add-autorefresh', function(data){
            updateResourceData(url, $item, data, docType, true);
            window.devtoolsWindow.saveMapping(url, data);
        });
        return false;
    });

    $('.remove-file', $item).click(function(){
        backgroundMsgSupport.removeDependency(url, $(this).attr('data-index'), function(data){
            updateResourceData(url, $item, data, docType, true);
            window.devtoolsWindow.saveMapping(url, data);
        });
        return false;
    });
}

var updateResourceOptions = function(url, $item, paths, docType){
    var $options = $('.resource-options', $item);

    if (paths.autoSave){
        //$('.auto-refresh-checkbox', $options).prop('checked', false);
        $('.auto-refresh-checkbox', $options).prop('checked', false);
        $('input', $options).prop('disabled', false);
    }
    else if (paths.autoReload && paths.autoReload.length){
        //$('.auto-save-checkbox', $options).prop('checked', false);
        $('.auto-refresh-checkbox', $options).prop('checked', true);
        $('input', $options).prop('disabled', false);
    }
    else{
        $('.auto-refresh-checkbox', $options).prop('checked', false).prop('disabled', true);
        //$('.auto-save-checkbox', $options).prop('checked', false).prop('disabled', true);
        $('.full-refresh-checkbox', $options).prop('disabled', true);
    }
    if (window.devtoolsWindow.isFullReload(url, docType)){
        $('.full-refresh-checkbox', $options).prop('checked', true);
    }
    else{
        $('.full-refresh-checkbox', $options).prop('checked', false);
    }
    if (docType == 'document'){
         $('.full-refresh-checkbox', $options).prop('checked', true).prop('disabled', true);
    }
}

var registerOptionChangeHandlers = function(url, $item, docType){
    var $autoRefeshCheckbox = $('.auto-refresh-checkbox', $item);
    var $fullRefreshCheckbox = $('.full-refresh-checkbox', $item);
    var updateAll = function(){
        backgroundMsgSupport.setResourceOptions(url, !$autoRefeshCheckbox[0].checked, function(paths){
            updateResourceData(url, $item, paths, docType, true);
            window.devtoolsWindow.saveMapping(url, paths);
        });
        return false;
    };
    $autoRefeshCheckbox.click(updateAll);
    $fullRefreshCheckbox.change(function(){
        window.devtoolsWindow.saveFullReloadMapping(url, this.checked);
    });
}

var refreshResources = function(resourceCache){
    var urlParser = document.createElement('a');
    var inspectedOrigin = devtoolsWindow.inspectedLocation.origin;
    var $resourceList = $('#resource-list-container');
    for (var i = 0; i < resourceCache.length; i++){
        var resource = resourceCache[i];
        var type = resource.type;
        if ((type != 'document' && type != 'script' && type != 'stylesheet') || (resource.url.indexOf(inspectedOrigin) != 0 && resource.url.indexOf('file://') != 0)){
            continue;
        }
        var $item = $('.resource[data-url="' + resource.url +'"]');
        if ($item.length){
            continue;
        }
        var template = '<div class="resource ' + type +'">\
            <div class="details">\
                <div class="resource-name">test.js</div>\
                <div class="resource-url">Url: /something/something/test.js</div>\
                <div class="resource-localpath">File Path: <span>None</span></div>\
                <div style="display:none" class="resource-reloadfrom">\
                    <ul>\
                    </ul>\
                </div>\
                <div class="resource-actions">\
                    <a href="#" class="browse-autosave-file">Set File</a>\
                </div>\
                <div class="resource-options">\
                    <label>\
                        <input name="resource-match-type' + i + '" class="auto-refresh-checkbox" type="checkbox" />\
                        <span>Generated from another language</span>\
                    </label>\
                    <label>\
                        <input class="full-refresh-checkbox" type="checkbox" />\
                        <span>Auto-Refresh performs a full refresh of the page</span>\
                    </label>\
                </div>\
            </div>\
        </div>';
        $item = $(template);

        urlParser.href = resource.url;
        var filename = urlParser.pathname.substring(urlParser.pathname.lastIndexOf('/') + 1);
        var url = urlParser.origin === inspectedOrigin ? resource.url.substring(inspectedOrigin.length) : resource.url;
        
        $item.appendTo($resourceList);
        
        $item.attr('data-url', resource.url);
        $('.resource-name', $item).text(filename.length ? filename : '/');
        $('.resource-url', $item).text('URL: ' + url);

        var paths = resource.paths || {};
        updateResourceData(resource.url, $item, paths, type, false);
        //updateResourceActions($item, paths);
        //updateResourceOptions($item, paths, type == 'document');
        
        registerOptionChangeHandlers(resource.url, $item, type);
    }
};

var initUI = function(){

    var typeSelect = document.getElementById('project-type');
    
    backgroundMsgSupport.getProjectTypes(function(types){
        projectTypes = types;
        for (var i = 0; i < projectTypes.length; ++i) {
            var projectType = projectTypes[i];
            typeSelect.add(new Option(projectType.name, i));
        }
        checkProjectState();
    });
    $(typeSelect).on('change', function(e){
        var index = Number(typeSelect.value);
        var projectType = projectTypes[index];
        if (projectType.locationType == 'local'){
            $('#file-select').show();
        }
    });
    $('#native-browse').on('click', function(e){
        var index = Number(typeSelect.value);
        backgroundMsgSupport.launchFolderSelect(index, window.devtoolsWindow.inspectedLocation.origin, function(result){
            if (result.path && result.path.length){
                $('#local-file-path').text(result.path).show();
                if (result.error){
                    logError(result.error);
                    $('#load-error').text(result.error).show();
                    $('#error-img').show();
                    $('#success-img').hide();
                    $('#load-success').hide();
                }
                else{
                    var projectType = projectTypes[index];
                    window.devtoolsWindow.loadProject(projectType.key, result.path, true, true, {});
                    $('#toggle-autosave')[0].checked = true;
                    $('#toggle-watch')[0].checked = true;
                    $('#auto-refresh').show();
                    $('#auto-save').show();
                    
                    $('#load-error').hide();
                    $('#error-img').hide();
                    $('#success-img').show();
                    $('#load-success').show();
                    $('#resource-section').show();
                }
            }
        });
        return false;
    });
    $('#resource-filter').on('search', function(e){
        var searchStr = this.value;
        if (searchStr && searchStr.length){
            $('.resource').hide();
            $('.resource').each(function(i, el){
                var resourceName = $('.resource-name', el).text();
                var resourceUrl = $('.resource-url', el).text();
                if (resourceName && resourceName.indexOf(searchStr) == 0){
                    $(el).show();
                }
                else{
                    var pathParts = resourceUrl.split('/');
                    for (var i = 0; i < pathParts.length; i++){
                        if (pathParts[i].indexOf(searchStr) == 0){
                            $(el).show();
                            break;
                        }
                    }
                }
            });
        }
        else{
            $('.resource').show();
        }
    });
    $('#toggle-watch').on('change', function(e){
        var path = $('#local-file-path').text();
        if (path && path.length){
            this.disabled=true;
            var self = this;
            window.devtoolsWindow.toggleWatchingFiles(this.checked, path, function(){
                self.disabled = false;
                window.devtoolsWindow.saveProjectState();
            });
        }
    });
    $('#toggle-autosave').on('change', function(e){
        if (window.devtoolsWindow.projectState){
            window.devtoolsWindow.projectState.autosave = this.checked;
        }
        window.devtoolsWindow.saveProjectState();
    });
    $('#toggle-logging').on('change', function(e){
        backgroundMsgSupport.setPreference({'logging': this.checked}, function(){});
        toggleLogging(this.checked);
        devtoolsWindow.toggleLogging(this.checked);
    });

    
    refreshResources(window.devtoolsWindow.resourceCache)
    //$('#resource-list-container').list()

};